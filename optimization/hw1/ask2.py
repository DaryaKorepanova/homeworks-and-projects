#ask 2
line_search_options = LineSearchTool(method='Wolfe')
plt.figure(figsize=(10, 10))
for n in [10, 100, 1000, 10000]:
    x_0 = list(np.zeros(n))
    
    average_num_it = []
    for j in range(5):
        num_it = []
        for k in range(0, 1000, 50):
            a = np.random.uniform(low=1, high=k+1, size = n - 2)
            a = np.hstack((a, np.array([1, k])))
            np.random.shuffle(a)
            A = scipy.sparse.csr_matrix(scipy.sparse.diags(a)).dot(np.eye(n))
            b = np.random.uniform(0, k, size=n)
            oracle = QuadraticOracle(A, b)

            [x_star, msg, history] = gradient_descent(oracle, x_0, line_search_options=line_search_options, trace=True)
            num_it.append(len(history['time']))
        plt.plot([i for i in range(0, 1000, 50)], num_it, '--', label='n = {}'.format(n))
        average_num_it.append(num_it)
    plt.plot([i for i in range(0, 1000, 50)], np.sum(np.array(average_num_it), axis=0))
plt.grid()
plt.legend()
plt.show()