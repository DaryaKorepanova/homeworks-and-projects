#oracles
import numpy as np
import scipy
from scipy.special import expit


class BaseSmoothOracle(object):
    """
    Base class for implementation of oracles.
    """

    def func(self, x):
        """
        Computes the value of function at point x.
        """
        raise NotImplementedError('Func oracle is not implemented.')

    def grad(self, x):
        """
        Computes the gradient at point x.
        """
        raise NotImplementedError('Grad oracle is not implemented.')

    def hess(self, x):
        """
        Computes the Hessian matrix at point x.
        """
        raise NotImplementedError('Hessian oracle is not implemented.')

    def func_directional(self, x, d, alpha):
        """
        Computes phi(alpha) = f(x + alpha*d).
        """
        return np.squeeze(self.func(x + alpha * d))

    def grad_directional(self, x, d, alpha):
        """
        Computes phi'(alpha) = (f(x + alpha*d))'_{alpha}
        """
        return np.squeeze(self.grad(x + alpha * d).dot(d))

    def hess_vec(self, x, v):
        """
        Computes matrix-vector product with Hessian matrix f''(x) v
        """
        return self.hess(x).dot(v)


class QuadraticOracle(BaseSmoothOracle):
    """
    Oracle for quadratic function:
       func(x) = 1/2 x^TAx - b^Tx.
    """

    def __init__(self, A, b):
        if not scipy.sparse.isspmatrix_dia(A) and not np.allclose(A, A.T):
            raise ValueError('A should be a symmetric matrix.')
        self.A = A
        self.b = b

    def func(self, x):
        return 0.5 * np.dot(self.A.dot(x), x) - self.b.dot(x)

    def grad(self, x):
        return self.A.dot(x) - self.b

    def hess(self, x):
        return self.A

    def minimize_directional(self, x, d):
        """
        Minimizes the function with respect to a specific direction:
            Finds alpha = argmin f(x + alpha d)
        """
        bd = np.dot(self.b, d)
        Axd = np.dot(self.matvecA_x(x), d)
        Add = np.dot(self.matvecA_x(d), d)
        return (bd - Axd) / Add
        pass

    def matvecA_x(self, x):
        return np.dot(self.A, x)

class LogRegL2Oracle(BaseSmoothOracle):
    """
    Oracle for logistic regression with l2 regularization:
         func(x) = 1/m sum_i log(1 + exp(-b_i * a_i^T x)) + regcoef / 2 ||x||_2^2.
    Let A and b be parameters of the logistic regression (feature matrix
    and labels vector respectively).
    For user-friendly interface use create_log_reg_oracle()
    Parameters
    ----------
        matvec_Ax : function
            Computes matrix-vector product Ax, where x is a vector of size n.
        matvec_ATy : function of y
            Computes matrix-vector product A^Ty, where y is a vector of size m.
        matmat_ATsA : function
            Computes matrix-matrix-matrix product A^T * Diag(s) * A,
    """

    def __init__(self, matvec_Ax, matvec_ATx, matmat_ATsA, b, regcoef):
        self.matvec_Ax = matvec_Ax
        self.matvec_ATx = matvec_ATx
        self.matmat_ATsA = matmat_ATsA
        self.b = b
        self.regcoef = regcoef

    def func(self, x):
        # TODO: Implement
        m = self.b.size
        ln = np.logaddexp(0, -self.b * self.matvec_Ax(x))
        return 1 / m * np.sum(ln) + self.regcoef / 2 * (np.linalg.norm(x) ** 2)

    def grad(self, x):
        # TODO: Implement
        m = self.b.size
        sigma = scipy.special.expit(self.b * self.matvec_Ax(x))
        return (1 / m) * self.matvec_ATx((sigma - 1) * self.b) + self.regcoef * x

    def pred_hess_v(self, x, v):
        m = self.b.size
        
        A_v = self.matvec_Ax(v)
        A_x = self.matvec_Ax(x)
        sigma = scipy.special.expit(self.matvec_Ax(x) * self.b)
        result = self.matvec_ATx(sigma * (np.ones(sigma.size) - sigma) * A_v) * 1 / m + self.regcoef * v
        return result

    def hess(self, x):
        # TODO: Implement
        m = self.b.size
        sigma = scipy.special.expit(self.matvec_Ax(x) * self.b)
        atsa = self.matmat_ATsA(sigma * (np.ones(sigma.size) - sigma))
        reg = self.regcoef * np.eye(x.size)
        return 1 / m * atsa + reg

    def hess_vec(self, x, v):
        """
        Computes matrix-vector product with Hessian matrix f''(x) v
        """
        return self.pred_hess_v(x, v)

class LogRegL2OptimizedOracle(LogRegL2Oracle):
    """
    Oracle for logistic regression with l2 regularization
    with optimized *_directional methods (are used in line_search).
    For explanation see LogRegL2Oracle.
    """

    def __init__(self, matvec_Ax, matvec_ATx, matmat_ATsA, b, regcoef):
        super().__init__(matvec_Ax, matvec_ATx, matmat_ATsA, b, regcoef)

    def func_directional(self, x, d, alpha):
        # TODO: Implement optimized version with pre-computation of Ax and Ad
        return None

    def grad_directional(self, x, d, alpha):
        # TODO: Implement optimized version with pre-computation of Ax and Ad
        return None


def create_log_reg_oracle(A, b, regcoef, oracle_type='usual'):
    """
    Auxiliary function for creating logistic regression oracles.
        `oracle_type` must be either 'usual' or 'optimized'
    """
    matvec_Ax = lambda x: A.dot(x)  # TODO: Implement
    matvec_ATx = lambda x: A.T.dot(x)  # TODO: Implement

    def matmat_ATsA(s):
        ATsA = matvec_ATx(np.diagflat(s))
        ATsA = scipy.sparse.csr_matrix(ATsA).dot(A)
        return ATsA

    if oracle_type == 'usual':
        oracle = LogRegL2Oracle
    elif oracle_type == 'optimized':
        oracle = LogRegL2OptimizedOracle
    else:
        raise 'Unknown oracle_type=%s' % oracle_type
    return oracle(matvec_Ax, matvec_ATx, matmat_ATsA, b, regcoef)


def hess_vec_finite_diff(func, x, v, eps=1e-5):
    """
    Returns approximation of the matrix product 'Hessian times vector'
    using finite differences.
    """
    # TODO: Implement numerical estimation of the Hessian times vector
    e = np.eye(x.size)
    result = np.zeros(x.size)
    for i in range(x.size):
        result[i] = (func(x + eps * v + eps * e[i]) - func(x + eps * v) - func(x + eps * e[i]) + func(x)) / (eps ** 2)
    return result
