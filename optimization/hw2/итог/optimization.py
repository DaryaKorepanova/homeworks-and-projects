import numpy as np
from collections import defaultdict, deque  # Use this for effective implementation of L-BFGS
from utils import get_line_search_tool
import time
import copy

def conjugate_gradients(matvec, b, x_0, tolerance=1e-4, max_iter=None, trace=False, display=False):
    """
    Solves system Ax=b using Conjugate Gradients method.
    Parameters
    ----------
    matvec : function
        Implement matrix-vector product of matrix A and arbitrary vector x
    b : 1-dimensional np.array
        Vector b for the system.
    x_0 : 1-dimensional np.array
        Starting point of the algorithm
    tolerance : float
        Epsilon value for stopping criterion.
        Stop optimization procedure and return x_k when:
         ||Ax_k - b||_2 <= tolerance * ||b||_2
    max_iter : int, or None
        Maximum number of iterations. if max_iter=None, set max_iter to n, where n is
        the dimension of the space
    trace : bool
        If True, the progress information is appended into history dictionary during training.
        Otherwise None is returned instead of history.
    display:  bool
        If True, debug information is displayed during optimization.
        Printing format is up to a student and is not checked in any way.
    Returns
    -------
    x_star : np.array
        The point found by the optimization procedure
    message : string
        'success' or the description of error:
            - 'iterations_exceeded': if after max_iter iterations of the method x_k still doesn't satisfy
                the stopping criterion.
    history : dictionary of lists or None
        Dictionary containing the progress information or None if trace=False.
        Dictionary has to be organized as follows:
            - history['time'] : list of floats, containing time in seconds passed from the start of the method
            - history['residual_norm'] : list of values Euclidian norms ||g(x_k)|| of the gradient on every step of the algorithm
            - history['x'] : list of np.arrays, containing the trajectory of the algorithm. ONLY STORE IF x.size <= 2
    """
    if max_iter is None:
        max_iter = b.size
    history = defaultdict(list) if trace else None
    x_k = np.copy(x_0)
    Ax_k = matvec(x_k)
    g_k = Ax_k - b
    d_k = -g_k
    Ad_k = matvec(d_k)
    start = time.time()

    for i in range(max_iter + 1):
        alpha_k = np.dot(g_k, g_k) / np.dot(d_k, Ad_k)

        if display and trace:
            print(history)
        if not trace and display:
            print('gy')

        if trace:
            history['time'].append(time.time() - start)
            history['residual_norm'].append(np.linalg.norm(g_k))       
            if x_k.size <= 2:
                history['x'].append(x_k)


        if (np.linalg.norm(g_k) < tolerance * np.linalg.norm(b)): # условие выхода
            return x_k, 'success', history
        if i == max_iter:
            return x_k, 'iterations_exceeded', history

        x_k = x_k + d_k * alpha_k
        tmp = g_k + alpha_k * Ad_k
        beta_k = np.dot(tmp, tmp) / np.dot(g_k, g_k)
        d_k = -tmp + d_k * beta_k 
        g_k = tmp
        Ad_k = matvec(d_k)  
    return x_k,'success', history



def bfgs_multiply(v, h_k, gamma_0):
    if len(h_k)== 0:
        return gamma_0 * v
    h_k_1 = copy.copy(h_k)
    s, y = h_k_1.pop()
    sv = np.dot(np.transpose(s), v)
    ys = np.dot(np.transpose(y), s)
    v = v - (sv / ys) * y
    z = bfgs_multiply(v, h_k_1, gamma_0)
    yz = np.dot(np.transpose(y), z)
    return z + ((sv - yz) / ys) * s

def lbfgs_direction(grad, h_k):
    if len(h_k) == 0:
        return -grad
    s, y = h_k[-1]
    gamma_0 = np.sum(y * s) / np.sum(y * y)
    return bfgs_multiply(-grad, h_k, gamma_0)

def lbfgs(oracle, x_0, tolerance=1e-4, max_iter=500, memory_size=10,
          line_search_options=None, display=False, trace=False):
    """
    Limited-memory Broyden–Fletcher–Goldfarb–Shanno's method for optimization.

    Parameters
    ----------
    oracle : BaseSmoothOracle-descendant object
        Oracle with .func() and .grad() methods implemented for computing
        function value and its gradient respectively.
    x_0 : 1-dimensional np.array
        Starting point of the algorithm
    tolerance : float
        Epsilon value for stopping criterion.
    max_iter : int
        Maximum number of iterations.
    memory_size : int
        The length of directions history in L-BFGS method.
    line_search_options : dict, LineSearchTool or None
        Dictionary with line search options. See LineSearchTool class for details.
    display : bool
        If True, debug information is displayed during optimization.
        Printing format is up to a student and is not checked in any way.
    trace:  bool
        If True, the progress information is appended into history dictionary during training.
        Otherwise None is returned instead of history.

    Returns
    -------
    x_star : np.array
        The point found by the optimization procedure
    message : string
        'success' or the description of error:
            - 'iterations_exceeded': if after max_iter iterations of the method x_k still doesn't satisfy
              the stopping criterion.
    history : dictionary of lists or None
        Dictionary containing the progress information or None if trace=False.
        Dictionary has to be organized as follows:
            - history['func'] : list of function values f(x_k) on every step of the algorithm
            - history['time'] : list of floats, containing time in seconds passed from the start of the method
            - history['grad_norm'] : list of values Euclidian norms ||g(x_k)|| of the gradient on every step of the algorithm
            - history['x'] : list of np.arrays, containing the trajectory of the algorithm. ONLY STORE IF x.size <= 2
    """
    history = defaultdict(list) if trace else None
    line_search_tool = get_line_search_tool(line_search_options)
    x_k = np.copy(x_0)

    grad_k = oracle.grad(x_k)
    norm_k = np.linalg.norm(grad_k)
    threshold = tolerance * norm_k ** 2
    start = time.time()
    h_k = deque([], memory_size)

    for k in range(max_iter + 1):
        if trace:
            history['time'].append(time.time() - start)
            history['func'].append(oracle.func(x_k))
            history['grad_norm'].append(norm_k)
            if x_k.shape[0] <= 2:
                history['x'].append(x_k)

        if display and trace:
            print("norm =", norm_k)
        if display and not trace:
            print('gy')

        if norm_k**2 <= threshold:
            return x_k, 'success', history

        if k == max_iter:
            return x_k, 'iterations_exceeded', history

        d_k = lbfgs_direction(grad_k, h_k)

        alpha_k = line_search_tool.line_search(oracle, x_k, d_k, 1)
        x_k_plus = x_k + alpha_k * d_k
        grad_k_plus = oracle.grad(x_k_plus)

        h_k.append((x_k_plus - x_k, grad_k_plus - grad_k))
        x_k = x_k_plus
        grad_k = grad_k_plus
        grad_k = oracle.grad(x_k)
        norm_k = np.linalg.norm(grad_k)

    return x_k, 'success', history

def hessian_free_newton(oracle, x_0, tolerance=1e-4, max_iter=500, 
                        line_search_options=None, display=False, trace=False):
    """
    Hessian Free method for optimization.
    Parameters
    ----------
    oracle : BaseSmoothOracle-descendant object
        Oracle with .func(), .grad() and .hess_vec() methods implemented for computing
        function value, its gradient and matrix product of the Hessian times vector respectively.
    x_0 : 1-dimensional np.array
        Starting point of the algorithm
    tolerance : float
        Epsilon value for stopping criterion.
    max_iter : int
        Maximum number of iterations.
    line_search_options : dict, LineSearchTool or None
        Dictionary with line search options. See LineSearchTool class for details.
    display : bool
        If True, debug information is displayed during optimization.
        Printing format is up to a student and is not checked in any way.
    trace:  bool
        If True, the progress information is appended into history dictionary during training.
        Otherwise None is returned instead of history.
    Returns
    -------
    x_star : np.array
        The point found by the optimization procedure
    message : string
        'success' or the description of error:
            - 'iterations_exceeded': if after max_iter iterations of the method x_k still doesn't satisfy
              the stopping criterion.
    history : dictionary of lists or None
        Dictionary containing the progress information or None if trace=False.
        Dictionary has to be organized as follows:
            - history['func'] : list of function values f(x_k) on every step of the algorithm
            - history['time'] : list of floats, containing time in seconds passed from the start of the method
            - history['grad_norm'] : list of values Euclidian norms ||g(x_k)|| of the gradient on every step of the algorithm
            - history['x'] : list of np.arrays, containing the trajectory of the algorithm. ONLY STORE IF x.size <= 2
    """
    if max_iter is None:
        max_iter = x_0.size
    history = defaultdict(list) if trace else None
    line_search_tool = get_line_search_tool(line_search_options)
    x_k = np.copy(x_0)
    grad_0 = oracle.grad(x_k)
    begin = time.time()

    message = 'success'
    for i in range(max_iter + 1):
        if display and trace:
            print(history)
        if not trace and display:
            print('gy')

        grad_k = oracle.grad(x_k)
        
        if trace:
            history['time'].append(time.time() - begin)
            history['func'].append(oracle.func(x_k))
            history['grad_norm'].append(np.linalg.norm(grad_k))
            if x_k.size <= 2:
                history['x'].append(x_k)

        if np.sum(grad_k ** 2) <= (tolerance * grad_0 ** 2).sum():
            break
        if i == max_iter:
            message = 'iterations_exceeded'
            break


        eta = min(0.5, np.sqrt(np.linalg.norm(grad_k)))
        d_k, _, _ = conjugate_gradients(lambda x: oracle.hess_vec(x_k, x), b=-grad_k, x_0=-grad_k, tolerance=eta)

        while np.dot(d_k, grad_k) >= 0:
            eta /= 10
            d_k, _, _ = conjugate_gradients(lambda x: oracle.hess_vec(x_k, x), b=-grad_k, x_0=d_k, tolerance=eta)

        alpha_k = line_search_tool.line_search(oracle, x_k, d_k, 1)
        x_k = x_k + alpha_k * d_k

    return x_k, message, history

def gradient_descent(oracle, x_0, tolerance=1e-5, max_iter=10000,
                     line_search_options=None, trace=False, display=False):
    """
    Gradien descent optimization method.
    Parameters
    ----------
    oracle : BaseSmoothOracle-descendant object
        Oracle with .func(), .grad() and .hess() methods implemented for computing
        function value, its gradient and Hessian respectively.
    x_0 : np.array
        Starting point for optimization algorithm
    tolerance : float
        Epsilon value for stopping criterion.
    max_iter : int
        Maximum number of iterations.
    line_search_options : dict, LineSearchTool or None
        Dictionary with line search options. See LineSearchTool class for details.
    trace : bool
        If True, the progress information is appended into history dictionary during training.
        Otherwise None is returned instead of history.
    display : bool
        If True, debug information is displayed during optimization.
        Printing format and is up to a student and is not checked in any way.
    Returns
    -------
    x_star : np.array
        The point found by the optimization procedure
    message : string
        "success" or the description of error:
            - 'iterations_exceeded': if after max_iter iterations of the method x_k still doesn't satisfy
                the stopping criterion.
            - 'computational_error': in case of getting Infinity or None value during the computations.
    history : dictionary of lists or None
        Dictionary containing the progress information or None if trace=False.
        Dictionary has to be organized as follows:
            - history['time'] : list of floats, containing time in seconds passed from the start of the method
            - history['func'] : list of function values f(x_k) on every step of the algorithm
            - history['grad_norm'] : list of values Euclidian norms ||g(x_k)|| of the gradient on every step of the algorithm
            - history['x'] : list of np.arrays, containing the trajectory of the algorithm. ONLY STORE IF x.size <= 2
    Example:
    --------
    >> oracle = QuadraticOracle(np.eye(5), np.arange(5))
    >> x_opt, message, history = gradient_descent(oracle, np.zeros(5), line_search_options={'method': 'Armijo', 'c1': 1e-4})
    >> print('Found optimal point: {}'.format(x_opt))
       Found optimal point: [ 0.  1.  2.  3.  4.]
    """
    history = defaultdict(list) if trace else None
    line_search_tool = get_line_search_tool(line_search_options)
    x_k = np.copy(x_0)

    # TODO: Implement gradient descent
    # Use line_search_tool.line_search() for adaptive step size.
    grad0 = oracle.grad(x_k)
    criterion = (tolerance * grad0 ** 2).sum()

    begin = time.time()

    for i in range(max_iter + 1):
        if display and trace:
            print(history)
        if not trace and display:
            print('gy')
        grad_k = oracle.grad(x_k)
        if trace:
            times = time.time() - begin
            func_k = oracle.func(x_k)
            norm_k = np.linalg.norm(grad_k)
            history['time'].append(times)
            history['func'].append(func_k)
            history['grad_norm'].append(norm_k)
            if x_k.size <= 2:
                history['x'].append(x_k)
        
        if np.any(grad_k is None) or np.any(grad_k == np.inf):
            return x_k, 'computational_error', history
            

        if (grad_k ** 2).sum() <= criterion:
            break
        if i == max_iter:
                return x_k, 'iterations_exceeded', history

        alpha_k = line_search_tool.line_search(oracle, x_k, -grad_k)
        if np.any(alpha_k) is None or np.any(alpha_k == np.inf):
            return x_k, 'computational_error', history

        x_k = x_k - alpha_k * grad_k

    
    return x_k, 'success', history