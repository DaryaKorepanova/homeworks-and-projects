import numpy as np
from scipy.special import expit
from sklearn.base import BaseEstimator
from numpy import random
import time
from scipy.spatial.distance import euclidean as euclid


class LogReg(BaseEstimator):
    def __init__(self, lambda_1=0.0, lambda_2=1.0, gd_type='stochastic',
                 tolerance=1e-4, max_iter=1000, w0=None, alpha=1e-3):
        """
        lambda_1: L1 regularization param
        lambda_2: L2 regularization param
        gd_type: 'full' or 'stochastic'
        tolerance: for stopping gradient descent
        max_iter: maximum number of steps in gradient descent
        w0: np.array of shape (d) - init weights
        alpha: learning rate
        """
        self.lambda_1 = lambda_1
        self.lambda_2 = lambda_2
        self.gd_type = gd_type
        self.tolerance = tolerance
        self.max_iter = max_iter
        self.w0 = w0
        self.alpha = alpha
        self.w = None
        self.loss_history = None
        self.time_history = None

    def fit(self, X, y):
        """
        X: np.array of shape (l, d)
        y: np.array of shape (l)
        ---
        output: self
        """
        if self.w0 is None:
            self.w0 = np.zeros(len(X[0]))
        self.w = self.w0
        self.loss_history = []
        self.time_history = []
        tmp_w = 0
        cntr = 0
        for i in range(self.max_iter):
            start = time.time()
            if self.gd_type == 'full':
                gradient = self.calc_gradient(X, y)
            else:
                num = random.randint(0, len(X[0]))
                gradient = self.calc_gradient(X[num], y[num])
            finish = time.time()
            tmp_w = self.w - self.alpha * gradient
            if np.all(self.w) != np.all(self.w0) and euclid(tmp_w, self.w) < self.tolerance:
                break
            self.loss_history.append(self.calc_loss(X, y))
            self.time_history.append(finish - start)
            self.w = tmp_w

        return self
        pass

    def predict_proba(self, X):
        """
        X: np.array of shape (l, d)
        ---
        output: np.array of shape (l, 2) where
        first column has probabilities of -1
        second column has probabilities of +1
        """
        if self.w is None:
            raise Exception('Not trained yet')
        else:
            predict = np.zeros((len(X), 2))
            predict[:, 0] = 1 - expit(np.dot(X, self.w))
            predict[:, 1] = expit(np.dot(X, self.w))
            return predict
        pass

    def calc_gradient(self, X, y):
        """
        X: np.array of shape (l, d) (l can be equal to 1 if stochastic)
        y: np.array of shape (l)
        ---
        output: np.array of shape (d)
        """
        ans = 1. / len(X) * np.dot((-y * expit(-y * np.dot(X, self.w))), X)
        ans += self.lambda_2 * self.w
        return ans
        pass

    def calc_loss(self, X, y):
        """
        X: np.array of shape (l, d)
        y: np.array of shape (l)
        ---
        output: float
        """
        ans = 0
        for i in range(len(X)):
            ans += np.logaddexp(0, -y[i] * np.dot(self.w, X[i]))
        ans *= 1. / len(X)
        ans += self.lambda_2 / 2 * np.dot(self.w, np.transpose(self.w))
        return ans
        pass
